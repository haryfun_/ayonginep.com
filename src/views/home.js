import React, {Component} from 'react'; 
import ResponsiveContainer from './ResponsiveContainer'
import Footers from './Footers'
import ContentRoom from './ContentRoom'


class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: []
        }
        this.handleSearchD = this.handleSearchD.bind(this)
    }

    handleSearchD(data){
        this.setState({data})
    }

    render(){
        
        return (
            <div> 
            <ResponsiveContainer handleSearch={this.handleSearchD}>
                <ContentRoom rooms={this.state.data}/>
                <Footers/>
    
            </ResponsiveContainer>
             
        </div> 
        )
    }
} 



export default Home; 