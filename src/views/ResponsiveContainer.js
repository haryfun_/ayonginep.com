import React, {Component} from 'react'
import DesktopContainer from './DesktopContainer'
import MobileContainer from './MobileContainer'

class ResponsiveContainer extends Component {
  constructor(props){
    super(props)
    this.handleSearchC = this.handleSearchC.bind(this)
  }

  handleSearchC(data){
    this.props.handleSearch(data)

  }
  render(){
    const {children} = this.props
    
    return (
      <div>
      <DesktopContainer handleSearch={this.handleSearchC}>{children}</DesktopContainer>
      <MobileContainer>{children}</MobileContainer>
    </div>
    )
  }
}

  
export default ResponsiveContainer