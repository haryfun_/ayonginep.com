import React, { Component } from 'react';
import { Button, Form, Modal, Segment, Icon, Divider, Input, Checkbox } from 'semantic-ui-react';
import { FormWithConstraints, FieldFeedbacks, FieldFeedback } from 'react-form-with-constraints';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import axios from 'axios';


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loginError: false,
      redirectToReferrer: false,
      splashScreen: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.contactSubmit = this.contactSubmit.bind(this);
  }

  handleChange(e) {
    this.form.validateFields(e.currentTarget);
    this.setState({ [e.target.name]: e.target.value });
  }

  contactSubmit(e) {
    e.preventDefault();

    this.form.validateFields();
    if (!this.form.isValid()) {
      console.log('form is invalid: do not submit');
    } else {
      console.log('form is valid: submit');
    }

    const { email, password } = this.state;

    axios.post('http://192.168.100.252:8000/oauth/token/', {
      email,
      password,
      client_id: 2,
      client_secret: 'WtkGX4tzOvD1JfA60Bj0Bwm79DZmvXzUsrJ3lrhJ',
      grant_type: 'password',
      newProvider: 'user'
    })
      .then(response => {
        if (response.status >= 200 && response.status < 300) {
          console.log(response);
          console.log('====================================');
          localStorage.setItem('token', `Bearer ${response.data.access_token}`);
          window.location.reload();
        } else {
          alert('Pastikan Email dan Password yang anda masukan benar.')
          window.location.reload();
        }
      }).catch(err => err);
  }

  render() {
    const { email, password } = this.state;

    const responseFacebook = (response) => {
      console.log(response);
      // this.signup(response, 'facebook');
    }
    const responseGoogle = (response) => {
      console.log(response);
      // this.signup(response, 'google');
    }
    const inlineStyle = {
      marginTop: '160px',
      marginLeft: 'auto',
      marginRight: 'auto'
    };
    return (
      <Modal size='tiny' style={inlineStyle} trigger={<Button inverted color='orange'>Masuk</Button>} closeIcon>
        <Segment>
          <FormWithConstraints ref={form => this.form = form} onSubmit={this.contactSubmit} noValidate>

            <Form.Field>
              <Divider horizontal >
                <Icon.Group size='big'>
                  <Icon circular inverted color='teal' name='user' />
                </Icon.Group></Divider>
              <Input type="email" name="email" icon='envelope' value={email} placeholder="Email"
                required onChange={this.handleChange} fluid />
              <FieldFeedbacks for="email" style={{ color: 'red' }}>
                <FieldFeedback when="*" style={{ color: 'red' }} />
              </FieldFeedbacks>
            </Form.Field>
            <Divider hidden />
            <Form.Field>
              <Input fluid type="password" name="password" value={password} icon='lock' placeholder="Password" onChange={this.handleChange} />
            </Form.Field> <Divider hidden />
            <Checkbox label='Ingat Saya' />
            <Divider hidden />
            <Button secondary fluid onClick={this.contactSubmit}>Login</Button>
            <Divider horizontal>Atau</Divider>

            <FacebookLogin
              appId="164795004215099"
              autoLoad
              callback={responseFacebook}
              render={renderProps => (
                <Button fluid color='facebook' onClick={renderProps.onClick}>
                  <Icon name='facebook' /> Facebook
                </Button>
              )}
            />
            <Divider fitted />
            <GoogleLogin
              clientId="677144338430-t04fkf3dehofbkvtb4ev26mrb8ugdc6q.apps.googleusercontent.com"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              render={renderProps => (
                <Button fluid color='google plus' onClick={renderProps.onClick}>
                  <Icon name='google' /> Google
                </Button>)}
            />

          </FormWithConstraints >
        </Segment>
      </Modal>
    );
  }
}
export default Login