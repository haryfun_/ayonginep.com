import React, { Component } from 'react'
import axios from 'axios'
import { Pagination } from 'semantic-ui-react'

let number = 0;
export default class Paginations extends Component {
    constructor() {
        super();
        this.state = {
            activePage: 1,
            result: [],
            data: [],
        }
        this.paginate = this.paginate.bind(this)
    }
    handlePaginationChange = (e, { activePage }) => this.componentDidMount(this.setState({ activePage }))

    componentDidMount(endp) {
        if (endp) {
            axios.get(`http://192.168.100.252:8000/api/rooms?page=${endp}`)
                .then(res => {
                    const data = res.data.data;
                    this.setState({ data });
                    this.setState({ result: data })
                    // this.props.handleSearch(data)
                    console.log('====================================');
                    console.log(data);
                    console.log('====================================');
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            axios.get(`http://192.168.100.252:8000/api/rooms`)
                .then(res => {
                    const data = res.data.data;
                    this.setState({ data });
                    this.setState({ result: data })
                    //   this.props.handleSearch(data)
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }

    paginate(e,{ activePage }) {

            let num = this.setState({activePage})
            console.log('====================================');
            console.log(num);
            console.log('====================================');
            this.componentDidMount(num)
            if(sort==='termurah'){
                this.componentDidMount('?price=low')
                console.log('termurah')
            }else{
                console.log('termahal')
                this.componentDidMount('?price=high')
            }
            
       
    }

    render() {
        const { activePage } = this.state
        return (
            <div>
                <Pagination
                    activePage={activePage}
                    onPageChange={this.paginate}
                    totalPages={5} />
            </div>
        )
    }
}
