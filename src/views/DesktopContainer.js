import React, { Component } from 'react';
import { Container, Menu, Responsive, Segment, Visibility,Dropdown, Image } from 'semantic-ui-react';
import { Parallax } from 'react-parallax';
import { Link } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import HomepageHeading from './HomepageHeading';
import Login from './Login';
import Register from './register';
import Profil from './profil';
import image1 from './asset/002.jpg';

class DesktopContainer extends Component {
  state = { 
    activeItem: 'home',
    link:''
  }
  constructor(props) {
    super(props)
    this.handleSearchB = this.handleSearchB.bind(this)
    this.logout = this.logout.bind(this)
    this.getImg = this.getImg.bind(this)
  }
  logout(){
    localStorage.removeItem('token');
    window.location.reload();
  }

  handleSearchB(data) {
    this.props.handleSearch(data)
  }
  
  getImg(params){
    this.setState({link:params})
  }
  

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })
  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { children } = this.props
    const { fixed } = this.state
    const { activeItem } = this.state

    return (

      <Responsive {...Responsive.onlyComputer}>
        <Parallax bgImage={image1} strength={500}>
          <div style={{ height: 600 }}>

            <Visibility style={{ top: '-3%', position: 'relative' }} once={false} onBottomPassed={this.showFixedMenu} onBottomPassedReverse={this.hideFixedMenu}>
              <Segment textAlign='center' vertical>

                <Menu
                  fixed={fixed ? 'top' : null}
                  inverted={!fixed}
                  pointing={!fixed}
                  secondary={!fixed}
                  size='large'
                >
                  <Container>

                    <Link to='/'><Menu.Item position='left' as='h2' >AyoNginep [dot] com</Menu.Item></Link>
                    <Menu.Item position='right'>
                      <Link onClick={this.handleItemClick} to='/'>
                        <Menu.Item name='home' active={activeItem === 'home'} />
                      </Link>
                      <Link onClick={this.handleItemClick} to='/'>
                        <Menu.Item name='Menjadi Tuan Rumah' active={activeItem === 'Menjadi Tuan Rumah'} />
                      </Link>
                      <Link onClick={this.handleItemClick} to='/'>
                        <Menu.Item name='Bantuan' active={activeItem === 'Bantuan'} />
                      </Link>
                      {localStorage.getItem('token') ?
                       
                          <Dropdown trigger={<Image avatar src={this.state.link} />} >
                            <Dropdown.Menu >
                              <Profil getImg={this.getImg}/>
                              <Dropdown.Divider />
                              <Dropdown.Item text='Log out' onClick={this.logout} />
                            </Dropdown.Menu>
                          </Dropdown>:
                        <div>
                          <Login inverted={!this.state.fixed} primary={!this.state.fixed} />
                          <Register inverted={!this.state.fixed} primary={!this.state.fixed} style={{ marginLeft: '0.5em' }}>Sign Up</Register>
                        </div>}
                    </Menu.Item>

                  </Container>
                </Menu>
              </Segment>
            </Visibility>
            <HomepageHeading handleSearch={this.handleSearchB} />
          </div>
        </Parallax>
        {children}
      </Responsive>
    )
  }
}

export default DesktopContainer