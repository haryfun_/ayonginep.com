import React, {Component} from 'react';
// import {Redirect} from 'react-router-dom';
import { Button, Form, Modal, Segment, Icon, Divider, Input,Checkbox } from 'semantic-ui-react';
import {FormWithConstraints,FieldFeedbacks,FieldFeedback} from 'react-form-with-constraints';
import {GoogleLogin } from 'react-google-login';
// import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import FacebookLogin from 'react-facebook-login';
import PostData from './PostData';
import axios from 'axios';
// import FacebookLogin from './FBloginButton'


class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email:'',
      password:'',
      redirect: false,
};
    this.handleChange = this.handleChange.bind(this);
    this.contactSubmit = this.contactSubmit.bind(this);
    this.signup = this.signup.bind(this)

  }

  signup(res,type){
    let postData;
    
     if (type === 'facebook' && res.email) {
     postData = {
          name: res.name,
          // provider: type,
          email: res.email,
          // provider_id: res.id,
          password: res.accessToken,
     };
    }

    if (type === 'google' && res.w3.U3) {
      postData = {
        name: res.w3.ig,
        // provider: type,
        email: res.w3.U3,
        // provider_id: res.El,
        password: res.Zi.access_token,
      };
    }

    if (postData) {
      PostData(postData).then((result) => {
         let responseJson = result;
         sessionStorage.setItem("userData", JSON.stringify(responseJson));
         this.setState({redirect: true});
      });
      } else {}
  }

  handleChange(e) {
    this.setState({[e.target.name]: e.target.value});
    this.form.validateFields(e.currentTarget);
  }

  contactSubmit(e) {
    e.preventDefault();
    const { email, password } = this.state;

    axios.post('http://192.168.100.252:8000/api/register/', { 
      email,
      password,
      first_name: '-',
      last_name: '-',
      c_password: password,
      birthday: '2021-12-21',
      client_id: 2,
      client_secret: 'WtkGX4tzOvD1JfA60Bj0Bwm79DZmvXzUsrJ3lrhJ',
      grant_type: 'password',
      newProvider:'user'
    })
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
          console.log(response);
          window.location.reload();
        } else {
          alert('Pastikan Email dan Password yang anda masukan benar.');
          window.location.reload();
        }
  }).catch(err => err);

    this.form.validateFields();
    if (!this.form.isValid()) {
      console.log('form is invalid: do not submit');
    } else {
      console.log('form is valid: submit');
    }
  }

  render() {
 
  const responseFacebook = (response) => {
    console.log("facebook console");
    console.log(response);
    this.signup(response, 'facebook');
}

const responseGoogle = (response) => {
    console.log("google console");
    console.log(response);
    this.signup(response, 'google');
}
    const inlineStyle = {
      marginTop: '130px',
      marginLeft: 'auto',
      marginRight: 'auto'
    };
    return (
      <Modal size='tiny' style={inlineStyle} trigger={<Button inverted color='blue'>Register</Button>} closeIcon>
        <Segment>
          <FormWithConstraints ref={form => this.form = form} >
              <Form.Field>
              <Divider horizontal >  
                <Icon.Group size='big'>
                  <Icon circular inverted color='teal' name='user'/>
                </Icon.Group></Divider>
                <Input fluid type="email" name="email" value={this.state.email} icon='envelope' placeholder="Email"
                  required onChange={this.handleChange} />
                <FieldFeedbacks for="email" style={{color:'red'}}>
                  <FieldFeedback when="*" style={{color:'red'}}/>
                </FieldFeedbacks>
              </Form.Field> 
              <Divider hidden />
              <Form.Field>
                <Input fluid type="password" name="password" value={this.state.password} icon='lock' placeholder="Password"
                  onChange={this.handleChange}
                  required pattern=".{6,}" />
                <FieldFeedbacks for="password">
                  <FieldFeedback when="valueMissing" />
                  <FieldFeedback when="patternMismatch" style={{color:'red'}}>
                    Harus berisi min. 6 karakter
                  </FieldFeedback>
                </FieldFeedbacks>
              </Form.Field>
              <Divider hidden />
              <Checkbox label='Ingat Saya' />
              <Divider hidden />
              <Button secondary fluid onClick={this.contactSubmit}>Daftar</Button>
              <Divider horizontal>Atau</Divider>
              
             
              <Divider fitted />
              <Button fluid color='google plus'>
                <Icon name='google' /> Google
              </Button>
              <Divider hidden />
          </FormWithConstraints >
          <FacebookLogin
                appId="164795004215099"
                autoLoad={true}
                fields="name,email"
                callback={responseFacebook} />
             
              <GoogleLogin
                clientId="677144338430-t04fkf3dehofbkvtb4ev26mrb8ugdc6q.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
              />
        </Segment>

      </Modal>
    );
  }
}
export default Register;