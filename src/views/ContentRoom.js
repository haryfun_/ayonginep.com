import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import {Link} from 'react-router-dom'
import {Button,Card,Image,Rating,Label,Divider,Grid,Segment,Reveal,Loader} from 'semantic-ui-react';
import Paginations from './Paginations'

export default class ContentRoom extends Component {

  constructor(props) {
    super(props)
    this.state = {
      active: false,
      rooms: []
    }
  }
  handleShow = () => this.setState({ active: true })
  handleHide = () => this.setState({ active: false })

  loader() {
    return this.props.rooms.length === 0 ?  <Loader active size='large'>Loading</Loader> :  <Loader disabled size='large' /> 
  }

  submitId(id) {
    this.props.updateSettings(id)
  }
  render() {
    return (
      <Segment raised style={{ padding: '3em 0em' }} vertical>
      {this.loader()}
        <Grid container stackable verticalAlign='middle'>
          <div>
            <Grid>
              {
                this.props.rooms.length
                  ?
                  this.props.rooms.map(person => {
                    return (<GridList person={person} key={person.id} />
                    )
                  }) : <div />}
            </Grid>
          </div>

          <Grid.Row>
            <Grid.Column textAlign='center'>
              <Divider horizontal inverted><Paginations handlePaginate={this.props.handleSearch}/></Divider>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    )
  }
}

const GridList = ({ person }) => (
  <Grid.Column mobile={16} tablet={8} computer={4} key={person.id}>

    <Card color='green' >
    <Link  to={{
            pathname: `/detailRoom/${person.id}`,
            state: {Id: person.id}
          }}>
      <Reveal animated='move up'>
        <Reveal.Content visible >
          <Image src={person.photos.length > 0 ? person.photos[0].image : 'https://roomshotels.com/wp-content/uploads/2016/09/DSC_4466-1024x683.jpg'} />
        </Reveal.Content>
        <Reveal.Content hidden>
          <Image src={person.photos.length > 0 ? person.photos[0].image : 'https://roomshotels.com/wp-content/uploads/2016/09/DSC_4466-1024x683.jpg'} />
        </Reveal.Content>
      </Reveal>
      </Link>
      <Card.Content extra>
        <Card.Header>{person.name}</Card.Header>Rating :
     <Rating maxRating={5} defaultRating={1} icon='star' size='mini' />
        <Card.Content>

          <Label color='red' ribbon='right'>Rp. {person.rent.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} /Malam</Label>
        </Card.Content>
        <Card.Meta>Bed : {person.room_capacities !== null ? person.room_capacities.bed : "-"}</Card.Meta>
        <Card.Description>{person.desc.slice(0, 50) + ` . . .`}</Card.Description>
      </Card.Content>
      <Card.Content></Card.Content>

      <Card.Content extra>
        
         
          <Link to={{
            pathname: `/detailRoom/${person.id}`,
            state: {Id: person.id}
          }}>
            <Button basic fluid color='red'>Lihat</Button>
          </Link>
       
      </Card.Content>
    </Card>

  </Grid.Column>
)
