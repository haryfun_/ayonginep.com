// import axios from 'axios';

export default function PostData(userData) {
    let BaseURL = 'https://192.168.100.10/api/register/';

    return new Promise((resolve, reject) => {
        fetch(BaseURL, {
            method: 'POST',
            headers: {
                "Content-Type": "text/plain"
            },
            body: JSON.stringify(userData)
        })
            .then((response) => response.json())
            .then((res) => {
                resolve(res);
            })
            .catch((error) => {
                reject(error);
            });

    // axios.post(BaseURL, {
    //     userData,
    //   })
    //   .then((response) => response.json())
    //     .then((res) => {
    //         resolve(res);
    //     })
    //     .catch((error) => {
    //         reject(error);
    //     });
    });
}



