import React,{Component} from 'react'
import axios from 'axios'
import {Input,Dropdown,Divider,Grid} from 'semantic-ui-react'

export default class Search extends Component{
    constructor(){
        super();
        this.state ={
            search:'',
            result:[],
            data:[],
        }
        this.updateSearch = this.updateSearch.bind(this)
        this.sortBy = this.sortBy.bind(this)
    }
   componentDidMount(endp){
        if (endp) {
            axios.get(`http://192.168.100.252:8000/api/rooms/${endp}`)
            .then(res => {
                const data = res.data.data;
                this.setState({data});
                this.setState({result:data})
                this.props.handleSearch(data)
            })
            .catch(function (error) {
                console.log(error);
            });
        } else {
            axios.get(`http://192.168.100.252:8000/api/rooms`)
            .then(res => {
              const data = res.data.data;
              this.setState({data});
              this.setState({result:data})
              this.props.handleSearch(data)
            })
            .catch(function (error) {
              console.log(error);
            });
        }
  }

    sortBy(sort){
        if(sort==='termurah'){
            this.componentDidMount('?price=low')
            console.log('termurah')
        }else{
            console.log('termahal')
            this.componentDidMount('?price=high')
        }
    //     
    }
    updateSearch(even){
        this.setState({search:even.target.value.substr(0,20)});

        let data = this.state.data.filter(datas=>{
            return datas.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !==-1 ;
          }
      )
      this.props.handleSearch(data)
    }
        
    render(){
  
        return(
            <div>
                 <Input size='small' fluid icon='search' placeholder='Cari kota...' 
                 value={this.state.search} onChange={this.updateSearch} />
                 <Divider hidden />
                 <Grid divided='vertically'>
                 <Grid.Row columns={2}>
                 <Grid.Column> 
                        <Dropdown placeholder='Urutkan' button fluid multiple >
                        <Dropdown.Menu>
                        <Dropdown.Item text='Termurah' onClick={()=>{
                            this.sortBy('termurah')
                        }}/>
                        <Dropdown.Item text='Termahal' onClick={()=>{
                            this.sortBy('termahal')
                        }}/>
                        <Dropdown.Item>Gratis</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Grid.Column>
                <Grid.Column> 
                        <Dropdown placeholder='Filter' button fluid multiple >
                        <Dropdown.Menu>
                        <Dropdown.Item>Paling Murah</Dropdown.Item>
                        <Dropdown.Item>Paling Mahal</Dropdown.Item>
                        <Dropdown.Item>Paling Populer</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Grid.Column>
                </Grid.Row>
                </Grid>
            </div>   
        )
    }
}
