import React, { Component } from 'react';
import { Container, Divider, Grid, Header, Image, List, Menu, Segment, Dropdown } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import ContentDetail from './ContentDetail';
import Login from '../Login';
import Register from '../register';
import Profil from '../profil';

class HomeDetail extends Component {

    state = { link: '' }
    constructor(props) {
        super(props);
        this.getImg = this.getImg.bind(this)
    }

    getImg(params) {
        this.setState({
            link: params
        })
    }

    render() {

        return (
            <div>
                <Menu fixed='top'>
                    <Container>
                        <Link to='/'>
                            <Menu.Item as='h3' header>AyoNginep [dot] com</Menu.Item>
                        </Link>
                        <Menu.Menu position='right'>
                            <Link to='/'><Menu.Item active>Home</Menu.Item></Link>
                            <Link to='/'><Menu.Item >Menjadi Tuan Rumah</Menu.Item></Link>
                            <Link to='/'><Menu.Item >Bantuan</Menu.Item></Link>
                            {localStorage.getItem('token') ?

                                <Dropdown trigger={<Image avatar src={this.state.link} />} >
                                    <Dropdown.Menu >
                                        <Profil getImg={this.getImg} />
                                        <Dropdown.Divider />
                                        <Dropdown.Item text='Log out' onClick={this.logout} />
                                    </Dropdown.Menu>
                                </Dropdown> :
                                <div>
                                    <Login />
                                    <Register style={{ marginLeft: '0em' }} />
                                </div>}

                        </Menu.Menu>
                    </Container>
                </Menu>

                <Container fluid style={{ marginTop: '0em', marginBottom: '0%', position: 'relative' }}>
                    <ContentDetail id={this.props.location.state} />

                    <Segment
                        inverted
                        vertical
                        style={{ margin: '1em 0em 0em', padding: '5em 0em' }}
                    >
                        <Container textAlign='center'>
                            <Grid divided inverted stackable>
                                <Grid.Row>
                                    <Grid.Column width={3}>
                                        <Header inverted as='h4' content='Group 1' />
                                        <List link inverted>
                                            <List.Item as='a'>Link One</List.Item>
                                            <List.Item as='a'>Link Two</List.Item>
                                            <List.Item as='a'>Link Three</List.Item>
                                            <List.Item as='a'>Link Four</List.Item>
                                        </List>
                                    </Grid.Column>
                                    <Grid.Column width={3}>
                                        <Header inverted as='h4' content='Group 2' />
                                        <List link inverted>
                                            <List.Item as='a'>Link One</List.Item>
                                            <List.Item as='a'>Link Two</List.Item>
                                            <List.Item as='a'>Link Three</List.Item>
                                            <List.Item as='a'>Link Four</List.Item>
                                        </List>
                                    </Grid.Column>
                                    <Grid.Column width={3}>
                                        <Header inverted as='h4' content='Group 3' />
                                        <List link inverted>
                                            <List.Item as='a'>Link One</List.Item>
                                            <List.Item as='a'>Link Two</List.Item>
                                            <List.Item as='a'>Link Three</List.Item>
                                            <List.Item as='a'>Link Four</List.Item>
                                        </List>
                                    </Grid.Column>
                                    <Grid.Column width={3}>
                                        <Header inverted as='h4' content='Footer Header' />
                                        <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>

                            <Divider inverted section />
                            <Image
                                centered
                                size='mini'
                                src='/logo.png'
                            />
                            <List horizontal inverted divided link>
                                <List.Item as='a' href='#'>Site Map</List.Item>
                                <List.Item as='a' href='#'>Contact Us</List.Item>
                                <List.Item as='a' href='#'>Terms and Conditions</List.Item>
                                <List.Item as='a' href='#'>Privacy Policy</List.Item>
                            </List>
                        </Container>
                    </Segment>
                </Container>
            </div>
        )
    }
}

export default HomeDetail