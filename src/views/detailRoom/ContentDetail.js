import React, { Component } from 'react'
import { Grid, Segment, Sticky, Form, Button, Item, Label, Divider, Rating, Container, Image, Icon } from 'semantic-ui-react'
import axios from 'axios';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker } from 'react-dates';
import GoogleMapReact from 'google-map-react';

const images = []
const AnyReactComponent = ({ text }) => <div>{text}</div>;
export default class ContentDetail extends Component {
  static defaultProps = {
    center: {
      lat: 27.6519425,
      lng: 137.422102
    },
    zoom: 11
  };
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      photoIndex: 0,
      isOpen: false,
      startDate: null,
      endDate: null,
      focusedInput: null,
    }
  }

  handleContextRef = contextRef => this.setState({ contextRef })

  componentDidMount() {

    axios.get(`http://192.168.100.252:8000/api/rooms/${this.props.id.Id}`)
      .then((response) => {
        const data = response.data;
        this.setState({ data: data })
      })
      .catch((error) => {
        console.log(error);
      });
  }
  
  render() {
    
    const startDates = this.state.startDate
    const endDates = this.state.endDate
    const room = this.state.data
    const { contextRef } = this.state
    const { photoIndex, isOpen } = this.state

    room.length ? room[0].photos.map(photo => {
      return images.push(photo.image)
    }) : console.log('-');
    return (

      <div className='ui fluid container' ref={this.handleContextRef}>
        {
          room.length > 0 ? <div key={room[0].id}>

            <Container fluid style={{ maxHeight: '400px', maxWidth: '100%' }}>
              <p as='a' onClick={() => this.setState({ isOpen: true })}>
                <Image style={{ zIndex: -1 }} src={room[0].photos.length ? room[0].photos[0].image : 'http://aditsa.net/directorio/wp-content/uploads/402976411-header-wallpapers.jpg'} fluid />
              </p>
            </Container>
            {isOpen && (
              <Lightbox
                mainSrc={images[photoIndex]}
                nextSrc={images[(photoIndex + 1) % images.length]}
                prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                onCloseRequest={() => this.setState({ isOpen: false })}
                onMovePrevRequest={() =>
                  this.setState({
                    photoIndex: (photoIndex + images.length - 1) % images.length
                  })}
                onMoveNextRequest={() =>
                  this.setState({
                    photoIndex: (photoIndex + 1) % images.length
                  })}
              />
            )}
            <Button onClick={() => this.setState({ isOpen: true })}
              style={{ zIndex: 5, marginTop: -330, marginRight: 25, boxShadow: "1px 1px 1px 0px #000" }}
              floated='right'><Icon name='heart' /> Simpan</Button>

            <Button onClick={() => this.setState({ isOpen: true })}
              style={{ zIndex: 5, marginTop: -330, marginRight: 150, boxShadow: "1px 1px 1px 0px #000" }}
              floated='right'><Icon name='search' />Tinjau Foto</Button>

            <Divider hidden />
            <Container fluid style={{ backgroundColor: '#fff' }}>
              <Grid container stackable >
                <Grid.Row>
                  <Grid.Column width={10}>
                    <Item>

                      <Item.Content>
                        <Item.Header as='a'><h1>{room[0].name}</h1></Item.Header>
                        <Item.Meta>
                          <span className='cinema'>{room[0].address_detail}</span>
                        </Item.Meta>
                        <Item.Extra>
                          {room[0].amenities.length ? room[0].amenities.map(facility => {
                            return facility.amenity_items ?
                              <Label icon='bathtub' key={facility.amenity_items.id} content={facility.amenity_items.item} />
                              : '-'
                          }) : '-'}
                        </Item.Extra>
                        Rating :
                        <Rating icon='star' defaultRating={3} maxRating={5} />
                        <Divider section />
                        <Item.Description style={{ fontSize: '1.1em' }}>{room[0].desc}</Item.Description>
                        <Divider section />
                        <Grid divided='vertically'>
                          <Grid.Row columns={2}>
                            <Grid.Column>
                              {room[0].amenities.length ? room[0].amenities.map(facility => {
                                return facility.amenity_items ? facility.amenity_items.item : '-'
                              }) : '-'}
                            </Grid.Column>
                            <Grid.Column>
                              {room[0].house_rules}===========
                              lat={parseFloat(room[0].coordinate.slice(0,room[0].coordinate.indexOf(',')))}======
                                  lng={parseFloat(room[0].coordinate.slice(room[0].coordinate.indexOf(',')+1))}
                            </Grid.Column>
                          </Grid.Row>
                          <Grid.Row columns={2}>
                            <Grid.Column>
                              {room[0].house_rules}
                            </ Grid.Column>
                          </Grid.Row>
                          <Grid.Row>
                            <Grid.Column style={{ height: '100vh', width: '100%' }}>
                            <GoogleMapReact
                                bootstrapURLKeys={{ key:'AIzaSyCdnLXZ2K-foDvgLkjKFMzIg7wjdXJdsZY'}}
                                defaultCenter={this.props.center}
                                defaultZoom={this.props.zoom}
                              >
                                <AnyReactComponent
                                  lat={parseFloat(room[0].coordinate.slice(0,room[0].coordinate.indexOf(',')))}
                                  lng={parseFloat(room[0].coordinate.slice(room[0].coordinate.indexOf(',')+1))}
                                  text={room[0].address_detail}
                                />
                              </GoogleMapReact>
                            </Grid.Column>
                          </Grid.Row>
                        </Grid>
                      </Item.Content>
                    </Item>

                  </Grid.Column>

                  <Grid.Column floated='right' width={6} style={{ backgroundColor: '#fff' }}>
                    <Sticky bottomOffset={50} context={contextRef} offset={50}  >
                      <Segment color='grey' raised>
                        <Form>
                          <Label as='h1' color='red' ribbon>Rp. {room[0].rent.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}/ Malam</Label><br />
                          <Divider hidden />

                          <h4>Tanggal </h4>

                          <DateRangePicker
                            startDateId="startDate"
                            endDateId="endDate"
                            startDate={startDates}
                            endDate={endDates}
                            startDatePlaceholderText="Check In"
                            endDatePlaceholderText="Check Out"
                            onDatesChange={({ startDate, endDate }) => this.setState({
                              startDate,endDate
                            })} 
                            focusedInput={this.state.focusedInput}
                            onFocusChange={(focusedInput) => { this.setState({ focusedInput }) }}
                          />
                          <Form.Field>
                          <Divider hidden />
                            <Button color='red' fluid>Pesan</Button>
                          </Form.Field>
                        </Form>
                      </Segment>
                    </Sticky>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Container>
          </div>
            : '-'

        }


      </div>

    )
  }
}