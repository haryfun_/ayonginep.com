import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import {Container,Header } from 'semantic-ui-react';
import Search from './search';

class HomepageHeading extends Component { 
constructor(props){
  super(props)

  this.handleSearchA = this.handleSearchA.bind(this)
}
handleSearchA(data){
  this.props.handleSearch(data)
}


render() {
  const {mobile} = this.props
  return(
    <Container text>
    <Header>
       <Header.Content as='h3'  style={{
         marginTop: mobile ? '0.5em' : '8em',
       }}></Header.Content>
     
     </Header>
     <Header>
       <Header.Content as='h3'  style={{
         color:'#fff',
         textShadow:'0.1em 0.1em 0.1em #000',
         fontFamily: 'Arial  ',
         fontSize: mobile ? '1.5em' : '2em',
         fontWeight: 'normal',
         textAlign:'center'
       }}>Temukan penginapan menarik di kota favoritmu.</Header.Content>
           <Search handleSearch={this.handleSearchA}/>
     </Header>
   </Container>
  )
}
}
  export default HomepageHeading;