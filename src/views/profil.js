import React, { Component } from 'react';
import { Button, Form, Modal, Segment, Icon, Input, Dropdown, Item, Table, TextArea, Image } from 'semantic-ui-react';
import axios from 'axios';
import usr from './asset/nouser.jpg';
const getToken = localStorage.getItem('token')

class Profil extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      email: '',
      password: '',
      first_name: '',
      last_name: '',
      gender: '',
      birthday: '',
      phone: '',
      address: '',
      about: '',
      photo: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.updateProfil = this.updateProfil.bind(this);
    this.onChange = this.onChange.bind(this);
    this.fileUpload = this.fileUpload.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  onUpdate(e) {
    e.preventDefault()
    this.fileUpload(this.state.photo)
  }

  onChange(e) {

    // let files = e.target.files || e.dataTransfer.files;
    //   if (!files.length)
    //         return;
    //   this.createImage(files[0]);
    this.setState({ photo: e.target.files[0] })
  }

  createImage(file) {
    let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        photo: e.target.result
      })
    };
    reader.readAsDataURL(file);
  }

  fileUpload(photo) {

    const url = 'http://192.168.100.252:8000/api/user/';
    // buf = new Buffer(req.body.imageBinary.replace(/^data:image\/\w+;base64,/, ""),'base64')
    const formData = new FormData();
    formData.append('photo', photo)
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
        'Authorization': getToken,
        //     ContentEncoding: 'base64',
        // ContentType: 'image/jpeg'
      }
    }
    return axios.post(url, formData, config).then(response => {
      console.log(response)
      window.location.reload()
    })
      .catch(error => {
        console.log(error.response)
      });
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  updateProfil(e) {
    e.preventDefault();
    const url = 'http://192.168.100.252:8000/api/user/';
    const { email, password, first_name, last_name, gender, birthday, phone, address, about } = this.state;

    axios.post(url, {
      email,
      password,
      first_name,
      last_name,
      gender,
      birthday,
      phone,
      address,
      about
    }, {
        'headers': { 'Authorization': getToken }
      })
      .then(response => {
        if (response.status >= 200 && response.status < 300) {
          alert('Data berhasil disimpan.')
          window.location.reload();
        } else {
          alert('Data gagal disimpan.');
        }
      }).catch(err => err);
  }

  componentDidMount() {
    const url = 'http://192.168.100.252:8000/api/me'
    axios.get(url, { 'headers': { 'Authorization': getToken } })
      .then((response) => {
        const getData = response.data[0]
        this.setState({
          email: getData.email ? getData.email : '',
          password: getData.password,
          first_name: getData.first_name,
          last_name: getData.last_name,
          gender: 'L',
          birthday: getData.birthday ? getData.birthday : '2012-12-12',
          phone: getData.phone ? getData.phone : '',
          address: getData.address ? getData.address : '',
          about: getData.about ? getData.about : '',
          photo: getData.photo ? getData.photo : usr
        })
        this.props.getImg(`http://192.168.100.252:8000/${this.state.photo}`)
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const inlineStyle = {
      marginTop: 'auto',
      marginLeft: 'auto',
      marginRight: 'auto'
    };
    return (
      <Modal size="small" style={inlineStyle} trigger={<Dropdown.Item text='Edit Profil' />} closeIcon>
        <Segment>
          <Item.Group>
            <Item>
              <Item.Content style={{textAlign:'center'}}>
                <Icon.Group >
                  <Image size='small' src={`http://192.168.100.252:8000/${this.state.photo}`} circular centered bordered style={{ boxShadow: '0.0em 0.0em 0.3em 0.3em #c0c0c0', textAlign: "center" }} /><br />
                  <Button as="label" htmlFor="file" size="tiny" ><Icon name="file" />Pilih Foto</Button>
                  <input id="file" hidden type="file" onChange={this.onChange} />
                  <Button size='tiny' positive content='Update Foto' onClick={this.onUpdate} />
                </Icon.Group>
                <Form onSubmit={this.updateProfil} >
                  <Form.Field style={{ textAlign: 'center' }}>
                    <Table color='red' >
                      <Table.Body >
                        <Table.Row >
                          <Table.Cell collapsing>Email</Table.Cell>
                          <Table.Cell>
                            <Input type="text" value={this.state.email} onChange={this.handleChange} name="email" />
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing>Password</Table.Cell>
                          <Table.Cell>
                            <Input fluid type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder="Ganti Password" />
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing>Nama Depan</Table.Cell>
                          <Table.Cell>
                            <Input type="text" name="first_name" value={this.state.first_name} onChange={(e) => this.setState({ first_name: e.target.value })} fluid />
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing>Nama Belakang</Table.Cell>
                          <Table.Cell>
                            <Input type="text" name="last_name" value={this.state.last_name} onChange={this.handleChange} fluid />
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing>Jenis Kelamin</Table.Cell>
                          <Table.Cell>
                            <Form.Dropdown name='gender' control="select" value={this.state.gender} onChange={this.handleChange} style={{ width: '100%' }}  >
                              <option value='l'>Laki-laki</option>
                              <option value='p'>Perempuan</option>
                            </Form.Dropdown >
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing>Tanggal Lahir</Table.Cell>
                          <Table.Cell>
                            <Input type="text" name="birthday" value={this.state.birthday} onChange={this.handleChange} fluid />
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing>Phone</Table.Cell>
                          <Table.Cell>
                            <Input type="text" name="phone" value={this.state.phone} onChange={this.handleChange} fluid />
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing >Alamat</Table.Cell>
                          <Table.Cell>
                            <TextArea name='address' value={this.state.address} onChange={this.handleChange} style={{ width: '100%' }} />
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing >Bio</Table.Cell>
                          <Table.Cell>
                            <TextArea name='about' value={this.state.about} onChange={this.handleChange} style={{ width: '100%' }} />
                          </Table.Cell>
                        </Table.Row>
                        <Table.Row >
                          <Table.Cell collapsing ></Table.Cell>
                          <Table.Cell textAlign='right'>
                            <Button positive onClick={this.updateProfil}  >Simpan</Button>
                          </Table.Cell>
                        </Table.Row>
                      </Table.Body>
                    </Table>
                  </Form.Field>
                </Form >
              </Item.Content>
            </Item>
          </Item.Group>

        </Segment>
      </Modal>
    );
  }
}
export default Profil



