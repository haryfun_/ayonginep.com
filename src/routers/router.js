import React from "react";
import { Route, Router} from 'react-router';
import Home from "../views/home";
import Login from "../views/Login";

const router = ()=>(
    <Router>
        <Route
            component={Home}
            path="/"
        />
        <Route
            component={Login}
            path="/login"
        />
    </Router>
)
export default router;

