import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,Route} from 'react-router-dom';
import './index.css';

import Home from './views/home';
import Login from './views/Login';
import HomeDetail from './views/detailRoom/home';
import registerServiceWorker from './registerServiceWorker';

const App=<Router>
<div>
<Route exact path="/" component={Home} />
<Route path="/detailRoom/:Id" component={HomeDetail} />
<Route path="/login" component={Login} />
</div>
</Router>

ReactDOM.render(App,document.getElementById('root'));
registerServiceWorker();
